package com.mission.demo.controller;

import com.mission.demo.service.IMyAsyncService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.concurrent.Future;

/**
 * @Auther huangliang
 * @Date 2022/03/29
 * @Describe 异步任务控制类
 */
@RestController
public class MyAsyncController {
    @Resource
    private IMyAsyncService myAsyncService;

    @GetMapping("/sendSMS")
    public String sendSMS() throws Exception{
        Long startTime = System.currentTimeMillis();
        myAsyncService.sendSMS();
        Long endTime = System.currentTimeMillis();

        System.out.println("主流程耗时:" + (endTime - startTime) + "ms");
        return "success";
    }

    @GetMapping("/statistics")
    public String statistics() throws Exception{
        Long startTime = System.currentTimeMillis();
        Future<Integer> futureA = myAsyncService.processA();
        Future<Integer> futureB = myAsyncService.processB();

        int total = futureA.get() + futureB.get();
        System.out.println("异步任务数据统计汇总结果:" + total + "ms");

        Long endTime = System.currentTimeMillis();
        System.out.println("主流程耗时：" + (endTime - startTime) + "ms");

        return "success";
    }
}
