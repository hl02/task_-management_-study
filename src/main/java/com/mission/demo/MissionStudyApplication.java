package com.mission.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

// 使用@EnableAsync注解开启基于注解的异步任务支持，该注解标注在项目启动类上
@EnableAsync
// 使用@EnableScheduling开启基于注解方式的定时任务支持，该注解标注在项目启动类上
@EnableScheduling
@SpringBootApplication
public class MissionStudyApplication {

    public static void main(String[] args) {
        SpringApplication.run(MissionStudyApplication.class, args);
    }

}
