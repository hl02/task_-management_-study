package com.mission.demo.service;

/**
 * @Auther huangliang
 * @Date 2022/03/29
 * @Describe 定制邮件发送业务接口
 */
public interface ISendEmailService {
    /**
     * 发送纯文本邮件
     * @param to 收件人地址
     * @param subject 邮件标题
     * @param text 邮件内容
     */
    void sendSimpleEmail(String to, String subject, String text);

    /**
     * 发送带附件和图片的邮件（静态资源+附件）
     * @param to 收件人地址
     * @param subject 邮件标题
     * @param text 邮件内容
     * @param filePath 附件地址
     * @param rscId 自定义的静态资源唯一标识，一个邮件内容中可能会包括多个静态资源，该属性用于区别唯一性
     * @param recPath 静态资源地址
     */
    void sendComplexEmail(String to, String subject, String text, String filePath, String rscId, String recPath);

    /**
     * @param to 收件人地址
     * @param subject 邮件标题
     * @param content 邮件内容
     */
    void sendTemplateEmail(String to, String subject, String content);
}
