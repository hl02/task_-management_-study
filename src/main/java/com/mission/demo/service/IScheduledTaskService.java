package com.mission.demo.service;

/**
 * @Auther huangliang
 * @Date 2022/03/29
 * @Describe 定时任务业务接口
 */
public interface IScheduledTaskService {
    void scheduledTaskImmediately();

    void scheduledTaskAfterSleep() throws InterruptedException;

    void scheduledTaskCron();
}
