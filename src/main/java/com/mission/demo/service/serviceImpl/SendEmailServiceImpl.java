package com.mission.demo.service.serviceImpl;

import com.mission.demo.service.ISendEmailService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;

// 在邮件发送过程中可能会出现多种问题，例如邮件发送过于频繁或者多次大批量的发送邮件，邮件可能会被邮件服务器拦截并识别为垃圾邮件，甚至是被拉入黑名单
@Service
public class SendEmailServiceImpl implements ISendEmailService {
    // 直接使用Spring Boot继承Spring框架的JavaMailSenderImpl实现类
    @Resource
    private JavaMailSenderImpl mailSender;

    @Value("${spring.mail.username}")
    private String from;
    public void sendSimpleEmail(String to, String subject, String text) {
        // 定制纯文本邮件信息SimpleMailMessage
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(from);
        message.setTo(to);
        message.setSubject(subject);
        // 邮件内容
        message.setText(text);
        try {
            // 发送邮件
            mailSender.send(message);
            System.out.println("纯文本邮件发送成功!");
        } catch (MailException e) {
            System.out.println("纯文本邮件发送失败" + e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void sendComplexEmail(String to, String subject, String text, String filePath, String rscId, String rscPath) {
        // 定制复杂邮件信息MimeMessage
        MimeMessage message = mailSender.createMimeMessage();
        try {
            // 使用MimeMessageHelper帮助类，并设置multipart多部件使用为true，对邮件进行信息进行封装处理，包括设置内嵌静态资源和邮件附件
            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            helper.setFrom(from);
            helper.setTo(to);
            helper.setSubject(subject);
            helper.setText(subject, true);

            // 设置邮件内嵌静态资源
            FileSystemResource res = new FileSystemResource(new File(rscPath));
            helper.addInline(rscId, res);

            // 设置邮件附件
            FileSystemResource file = new FileSystemResource(new File(filePath));
            String fileName = filePath.substring(filePath.lastIndexOf(File.separator));
            helper.addAttachment(fileName, file);

            // 发送邮件
            mailSender.send(message);
            System.out.println("复杂邮件发送成功！");
        } catch (MessagingException e) {
            System.out.println("复杂邮件发送失败" + e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void sendTemplateEmail(String to, String subject, String content) {
        MimeMessage message = mailSender.createMimeMessage();
        try {
            // 使用MimeMessageHelper帮助类，并设置multipart多部件使用为true，对邮件进行信息进行封装处理
            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            helper.setFrom(from);
            helper.setTo(to);
            helper.setSubject(subject);
            helper.setText(content, true);

            //发送邮件
            mailSender.send(message);
            System.out.println("模板邮件发送成功!");
        } catch (MessagingException e) {
            System.out.println("模板邮件发送失败" + e.getMessage());
            e.printStackTrace();
        }
    }
}