package com.mission.demo.service.serviceImpl;

import com.mission.demo.service.IScheduledTaskService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;

@Service
public class ScheduledTaskServiceImpl implements IScheduledTaskService {
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private Integer count1 = 1;
    private Integer count2 = 2;
    private Integer count3 = 3;

    @Scheduled(fixedRate = 60000)
    public void scheduledTaskImmediately(){
        System.out.printf("fixedRate第%s次执行，当前时间为：%s%n", count1++, dateFormat.format(new Date()));
    }

    @Scheduled(fixedDelay = 60000)
    public void scheduledTaskAfterSleep() throws InterruptedException{
        System.out.printf("fixedDelay第%s次执行，当前时间为：%s%n", count2++, dateFormat.format(new Date()));
        // 模拟该定时任务处理耗时为10秒
        Thread.sleep(10000);
    }

    @Scheduled(cron = "0 * * * * *")
    public void scheduledTaskCron(){
        System.out.printf("cron第%s次执行，当前时间为：%s%n", count3++, dateFormat.format(new Date()));
    }
}
