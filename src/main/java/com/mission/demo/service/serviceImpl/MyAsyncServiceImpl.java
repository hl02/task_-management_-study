package com.mission.demo.service.serviceImpl;

import com.mission.demo.service.IMyAsyncService;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import java.util.concurrent.Future;

@Service
public class MyAsyncServiceImpl implements IMyAsyncService {
    // 使用@Async注解将sendSMS方法标注为异步方法，用于模拟发送短信验证码
    @Async
    public void sendSMS() throws Exception{
        System.out.println("调用短信验证码业务方法...");
        Long startTime = System.currentTimeMillis();
        Thread.sleep(5000);
        Long endTime = System.currentTimeMillis();
        System.out.println("短信业务执行完成耗时：" + (endTime - startTime) + "ms");
    }

    @Async
    public Future<Integer> processA() throws Exception{
        System.out.println("开始分析并统计业务A数据");
        Long startTime = System.currentTimeMillis();
        Thread.sleep(4000);
        // 模拟定义一个假的统计结果
        int count = 123456;
        Long endTime = System.currentTimeMillis();
        System.out.println("业务A数据统计耗时：" + (endTime - startTime) + "ms");
        return new AsyncResult<Integer>(count);
    }

    @Async
    public Future<Integer> processB() throws Exception{
        System.out.println("开始分析并统计业务B数据");
        Long startTime = System.currentTimeMillis();
        Thread.sleep(5000);
        // 模拟定义一个假的统计结果
        int count = 654321;
        Long endTime = System.currentTimeMillis();
        System.out.println("业务B数据统计耗时：" + (endTime - startTime) + "ms");
        return new AsyncResult<Integer>(count);
    }
}
