package com.mission.demo.service;

import java.util.concurrent.Future;

/**
 * @Auther huangliang
 * @Date 2022/03/29
 * @Describe 异步任务调用业务接口
 */
public interface IMyAsyncService {
    /**
     * 无返回值
     * @throws Exception
     */
    void sendSMS() throws Exception;

    /**
     * 有返回值
     * @return
     * @throws Exception
     */
    Future<Integer> processA() throws Exception;

    /**
     * 有返回值
     * @return
     * @throws Exception
     */
    Future<Integer> processB() throws Exception;
}
