package com.mission.demo;

import com.mission.demo.service.ISendEmailService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.annotation.Resource;

// 使用@EnableAsync注解开启基于注解的异步任务支持，该注解标注在项目启动类上
@EnableAsync
// 使用@EnableScheduling开启基于注解方式的定时任务支持，该注解标注在项目启动类上
@EnableScheduling
@SpringBootTest
class MissionStudyApplicationTests {
    @Resource
    private ISendEmailService sendEmailService;

    @Resource
    private TemplateEngine templateEngine;

    @Test
    @DisplayName("发送纯文本邮件")
    public void sendSimpleMailTest(){
        String to = "1211268379@qq.com";
        String subject = "【花开有声，花落无声】";
        String text = "Spring Boot纯文本邮件发送测试内容......";

        // 发送纯文本邮件
        sendEmailService.sendSimpleEmail(to,subject,text);
    }

    @Test
    @DisplayName("发送带静态资源图片和附件的复杂邮件")
    public void sendComplexEmailTest(){
        String to = "1211268379@qq.com";
        String subject = "【爱意东升西落，浪漫至死不渝】";

        // 定义邮件内容
        StringBuilder text = new StringBuilder();
        text.append("<html><head></head>");
        text.append("<body><h1>我把你丢失在人海里，下落不明，了无音讯</h1>");

        // cid为固定写法，rscId自定义的资源和你唯一标识
        String rscId = "img001";
        text.append("<img src='cid:" +rscId+"'/></body>");
        text.append("</html>");

        // 指定静态资源文件和附件路径
        String rscPath = "D:\\桌面\\图像库\\公路.jpg";
        String filePath = "D:\\桌面\\工作\\个人简历\\吴功龙个人简历.pdf";

        // 发送复杂邮件
        sendEmailService.sendComplexEmail(to,subject,text.toString(),filePath, rscId, rscPath);
    }

    @Test
    @DisplayName("发送模板邮件")
    public void sendTemplateEmailTest(){
        String to = "1211268379@qq.com";
        String subject = "【时间沦陷】";

        // 使用模板邮件定制邮件正文内容
        Context context = new Context();
        context.setVariable("username", "黄亮");
        context.setVariable("code", "456123");

        // 使用TemplateEngine解析要处理的模板页面
        // 第一个参数指要解析的Thymeleaf模板页面，第二个参数用于设置页面中的动态数据
        String emailContent = templateEngine.process("emailTemplate_vercode", context);

        // 发送模板邮件
        sendEmailService.sendTemplateEmail(to,subject,emailContent);
    }
}
